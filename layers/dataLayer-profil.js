var MongoClient = require('mongodb').MongoClient;
var url = "mongodb+srv://Titi:mdpsupersecret@polytech-irfyo.gcp.mongodb.net/appweb?retryWrites=true";
var client = new MongoClient(url,{useNewUrlParser: true});
var db;
var ObjectID = require('mongodb').ObjectID;


var dataLayer = {
    init : function(){
        client.connect(function(err){
            if(err) throw err;
            db = client.db("appweb");
        })
    },

    find : function(auteur, cb){
        
        db.collection("Liste").find(auteur).toArray(function(err, lists){
            if(err) throw err;
            cb(lists);
        });
    },

    findCollab : function(auteur, cb){
        db.collection("Liste").find(auteur).toArray(function(err, lists){
            if(err) throw err;
            cb(lists);
        });
    },

    create : function(list, cb){
        db.collection("Liste").insertOne(list, function(err, result){
            if(err) throw err;
            cb();
        });
    },

    addCollab : function(list, collab, cb){
        list = new ObjectID(list);
        db.collection("Liste").updateOne({_id : list}, { $push: { collabo: collab } }, function(err, result){
            if(err) throw err;
            cb();
        });
    },

    removeCollab : function(list, collab, cb){
        list = new ObjectID(list);
        db.collection("Liste").updateOne({_id : list}, { $pull: { collabo: collab } }, function(err, result){
            if(err) throw err;
            cb();
        });
    },

    deleteOne : function(list, cb){
        list = new ObjectID(list._id);
        db.collection("Liste").deleteOne({_id : list}, function(err, results) {
            if(err) throw err;
            cb();
        });
    },

    updateOne : function(oldlist, newlist, cb){
        oldlist = new ObjectID(oldlist._id)
        db.collection("Liste").updateOne({_id : oldlist}, newlist, function(err, result){
            if(err) throw err;
            cb();
        });
    }
};

module.exports = dataLayer;