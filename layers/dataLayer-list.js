var MongoClient = require('mongodb').MongoClient;
var url = "mongodb+srv://Titi:mdpsupersecret@polytech-irfyo.gcp.mongodb.net/appweb?retryWrites=true";
var client = new MongoClient(url,{useNewUrlParser: true});
var db;
var ObjectID = require('mongodb').ObjectID;


var dataLayer = {
    init : function(cb){
        client.connect(function(err){
            if(err) throw err;
            db = client.db("appweb");
            cb();
        })
    },

    find : function(list,cb){
        db.collection("Tache").find(list).toArray(function(err, tasks){
            if(err) throw err;
            cb(tasks);
        });
    },

    create : function(task, cb){
        db.collection("Tache").insertOne(task, function(err, result){
            if(err) throw err;
            cb();
        });
    },

    deleteOne : function(task, cb){
        console.log(task)
        task = new ObjectID(task._id);
        
        db.collection("Tache").deleteOne({_id : task}, function(err, results) {
            if(err) throw err;
            cb();
        });
    },

    delete : function(list, cb){
        db.collection("Tache").delete(list, function(err, results) {
            if(err) throw err;
            cb();
        });
    },

    updateOne : function(oldtask, newtask, cb){
        oldtask = new ObjectID(oldtask._id)
        db.collection("Tache").updateOne({_id : oldtask}, newtask, function(err, result){
            if(err) throw err;
            cb();
        });
    }
};

module.exports = dataLayer;