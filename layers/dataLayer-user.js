var MongoClient = require('mongodb').MongoClient;
var url = "mongodb+srv://Titi:mdpsupersecret@polytech-irfyo.gcp.mongodb.net/appweb?retryWrites=true";
var client = new MongoClient(url,{useNewUrlParser: true});
var db;
var ObjectID = require('mongodb').ObjectID;


var dataLayer = {
    init : function(){
        client.connect(function(err){
            if(err) throw err;
            db = client.db("appweb");
        })
    },

    find : function(cb){
        db.collection("User").find({}).toArray(function(err, users){
            if(err) throw err;
            cb(users);
        });
    },

    findOne : function(user, cb){

        db.collection("User").findOne(user, function(err, user){
            if(err) throw err;
            cb(user);
        });
    },

    getID : function(user, cb){

        db.collection("User").findOne(user, {projection: { _id: true }}, function(err, user){
            if(err) throw err;
            cb(user);
        });
    },

    create : function(user, cb){
        db.collection("User").insertOne(user, function(err, result){
            if(err) throw err;
            cb();
        });
    },

    deleteOne : function(user, cb){

        user = new ObjectID(user._id);
        
        db.collection("User").deleteOne({_id : user}, function(err, results) {
            if(err) throw err;
            cb();
        });
    },

    updateOne : function(olduser, newuser, cb){

        olduser = new ObjectID(olduser._id)

        db.collection("User").updateOne({_id : olduser}, newuser, function(err, result){
            if(err) throw err;
            cb();
        });
    }
};

module.exports = dataLayer;