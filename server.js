var dataLayerList = require('./layers/dataLayer-list');
var dataLayerUser = require('./layers/dataLayer-user');
var dataLayerProfil = require('./layers/dataLayer-profil');

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var routeList = require('./routes/api-route-list');
var routeUser = require('./routes/api-route-user');
var routeProfil = require('./routes/api-route-profil');
 
app.use(express.static(__dirname +'/public/'));
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());
app.use(bodyParser.json({tpe: 'application/vnd.api+json'}));
app.use(cookieParser());

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-with, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
  });


routeList(app);
routeUser(app);
routeProfil(app);

const port = process.env.PORT || 8088;

dataLayerList.init(function(){
    app.listen(port);
    console.log("on utilise le port " + port);
});

dataLayerUser.init();
dataLayerProfil.init();