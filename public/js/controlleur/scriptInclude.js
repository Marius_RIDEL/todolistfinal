monApp.controller('includeCtrl',['$scope','$cookies','$http', '$state', function($scope,$cookies,$http,$state){
    var user = $cookies.getObject('user');
    if(!user){
        page = "connection";
    }else{
        page = 'profil';
    }
    $state.go(page);
}]);