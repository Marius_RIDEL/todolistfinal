monApp.controller('connectionCtrl',['$scope','$http','$cookies','$state', function($scope,$http,$cookies,$state){
    $scope.formData = {};

    $scope.demandeConnexion = function(){

        if ($scope.formData.userName != undefined && $scope.formData.password != undefined) {

            $http({
                method: 'POST',
                url: '/api/user/find',
                data: $scope.formData
            }).then(function successCallback(response){
                $scope.formData = {};
                
                $cookies.putObject('user',response.data);
                connecte = true;

                $state.go('profil');
            },
            function errorCallback(response){
                $("#log").attr( "class", "text-danger" );
                $("#log").text(response.data);
            });
        }
        else{
            $("#log").attr( "class", "text-danger" );
            $("#log").text("Veuillez remplir tout les champs");
        }

        
    };

    $scope.inscription = function(){

        if ($scope.formData.userName != undefined && $scope.formData.password != undefined) { 

             $http({
                method: 'POST',
                url: '/api/user/create',
                data: $scope.formData
            }).then(function successCallback(response){
                $scope.formData = {};
                $("#log").attr( "class", "text-success" );
                $("#log").text(response.data);

            },
            function errorCallback(response){
                $("#log").attr( "class", "text-danger" )
                $("#log").text(response.data);
            });
        }
        else{
            $("#log").attr( "class", "text-danger" );
            $("#log").text("Veuillez remplir tout les champs");
        }

    };
    
}]);