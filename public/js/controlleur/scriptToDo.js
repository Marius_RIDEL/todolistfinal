
monApp.controller('toDoCtrl',['$scope','$http','$cookies','$stateParams', '$state',function($scope,$http,$cookies,$stateParams,$state){
    
    $scope.formData = {};
    console.log($stateParams);
    $scope.listID = { listID : $stateParams.listeID};
    $scope.listName = { listName : $stateParams.listeName};
    if($scope.listID.listID == null)$state.go('profil');

    $http({
        method: 'post',
        url: '/api/laliste',
        data: $scope.listID
    }).then(function successCallback(response){
        $scope.laliste = response.data;
    },
    function errorCallback(response){
        console.log(response.data);
    });

    $scope.addTask = function (){
        if ($scope.formData.taskName != undefined) {
            $scope.formData.listID = $scope.listID.listID;
            $scope.formData.auteur = $cookies.getObject('user').userName;
            $http({
                method: 'post',
                url: '/api/laliste/create',
                data: $scope.formData
            }).then(function successCallback(response){
                $scope.formData ={};
                $scope.laliste = response.data;
            },
            function errorCallback(response){
                console.log(response.data);
            });
        }
    };

    $scope.removeTask = function (id){
        var req = {
            _id: id,
            listID: $scope.listID.listID
        }
        $http({
            method: 'post',
            url: '/api/laliste/delete',
            data: req
        }).then(function successCallback(response){
            $scope.laliste = response.data;
        },
        function errorCallback(response){
            console.log(response.data);
        });
    };

    $scope.doneTask = function (id){
        var req = {
            _id: id,
            listID: $scope.listID.listID
        }

        $http({
            method: 'put',
            url: '/api/laliste/done',
            data: req
        }).then(function successCallback(response){
            $scope.laliste = response.data;
        },
        function errorCallback(response){
            console.log(response.data);
        });
    };

    $scope.updateTask = function (id, newName){
        if (newName != "") {
            var req = {
                _id: id,
                newName: newName,
                auteur: $cookies.getObject('user').userName,
                listID: $scope.listID.listID
            }
            $http({
                method: 'put',
                url: '/api/laliste/update',
                data: req
            }).then(function successCallback(response){
                $scope.laliste = response.data;
            },
            function errorCallback(response){
                console.log(response.data);
            });
        }
    };
}]);
