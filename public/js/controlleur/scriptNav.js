monApp.controller('navCtrl',['$scope','$cookies','$state', function($scope,$cookies,$state){
    
    $scope.userName = $cookies.getObject('user').userName;

    $scope.retourChoixListe = function(){
        $state.go('profil');
    };

    $scope.deconnexion = function(){
        $cookies.remove('user');
        $state.go('connection');
    };
}]);