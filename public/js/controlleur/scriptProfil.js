monApp.controller('profilCtrl',['$scope','$cookies','$http', '$state', function($scope,$cookies,$http,$state){

    $scope.idUser = $cookies.getObject('user')._id;
    $scope.userName = $cookies.getObject('user').userName;

    $scope.formData = {};

    $http({
        method: 'get',
        url: '/api/leslistes/perso'
    }).then(function successCallback(response){
        $scope.liste = response.data;
    },
    function errorCallback(response){
        console.log(response.data);
    });

    $http({
        method: 'get',
        url: '/api/leslistes/collab'
    }).then(function successCallback(response){
        $scope.listec = response.data;
    },
    function errorCallback(response){
        console.log(response.data);
    });

    $scope.monModal = function (id, isClosed){
        if(isClosed){
            $(id).show();
        } 
        else {
            $(id).hide();
        }
    };

    $scope.addList = function (){
        if ($scope.formData.listName != undefined) {
            $scope.formData.auteur = $scope.idUser;
            $http({
                method: 'post',
                url: '/api/leslistes/create',
                data: $scope.formData
            }).then(function successCallback(response){
                $scope.formData ={};
                $scope.liste = response.data;
            },
            function errorCallback(response){
                console.log(response.data);
            });
        }
    };

    $scope.removeList = function (id){
        var req = {
            _id: id,
        }
        $http({
            method: 'post',
            url: '/api/leslistes/delete',
            data: req
        }).then(function successCallback(response){
            $scope.liste = response.data;
        },
        function errorCallback(response){
            console.log(response.data);
        });
    };

    $scope.addCollabo = function (list){
        
        if ($scope.formData.collabo != undefined) {
           
            var data = {_id : list._id ,collabo : $scope.formData.collabo};
            
            $http({
                method: 'post',
                url: '/api/user/exist',
                data: data
            }).then(function successCallback(response){
                
                if(!list.collabo.includes(data.collabo) && data.collabo != $scope.userName){
                    if(response.data){

                        $http({
                            method: 'put',
                            url: '/api/leslistes/addCollab',
                            data: data
                        }).then(function successCallback(response){
                            $scope.formData ={};
                            $scope.liste = response.data;
                        },
                        function errorCallback(response){
                            console.log(response.data);
                        });

                    } else {
                        $scope.formData ={};
                        $("#log").text("Utilisateur inexistant.");
                    }

                } else {
                    $scope.formData ={};
                    $("#log").text("Collaborateur déja présent.");
                }

            },
            function errorCallback(response){
                console.log(response.data);
            });

        }
    };

    $scope.removeCollabo = function (list,user){;
        var data = {_id : list ,collabo : user};
        $http({
            method: 'put',
            url: '/api/leslistes/removeCollab',
            data: data
        }).then(function successCallback(response){
            if(data.collabo == $scope.userName){
                $scope.listec = response.data;
            } else {
                $scope.liste = response.data;
            }
            
        },
        function errorCallback(response){
            console.log(response.data);
        });
    };

}]);