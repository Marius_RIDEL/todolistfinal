var monApp = angular.module('monApp', ['ui.router','ngCookies']);

monApp.config(function($locationProvider, $stateProvider, $urlRouterProvider){
    $locationProvider.hashPrefix('');

    var includeState = {
        name: "include",
        url:"/",
        views: {
            'header@': {
            },
            'content@': {
                controller: "includeCtrl"
            }
        }
    };

    var connectionState = {
        name: "connection",
        url:"/",
        views: {
            'header@': {

            },
            'content@': {
                templateUrl: "connection.html",
                controller: "connectionCtrl"
            }
        }
    };
    
    var todoState = {
        name: "todo",
        url:"/",
        params: {
            listeID: null,
            listeName: null
        },
        views: {
            'header@': {
                templateUrl: 'navbar.html',
                controller: 'navCtrl'
            },
            'content@': {
                templateUrl: "todo.html",
                controller: "toDoCtrl"
            }
        }
    };

    var profilState = {
        name: "profil",
        url:"/",
        views: {
            'header@': {
                templateUrl: 'navbar.html',
                controller: 'navCtrl'
            },
            'content@': {
                templateUrl: 'profil.html',
                controller: 'profilCtrl'
            }
        }
    };

    var erreurState = {
        name: "404",
        url:"/",
        views: {
            'header@': {
            },
            'content@': {
                templateUrl: "404.html",
            }
        }
    };

    $stateProvider.state(includeState);
    $stateProvider.state(connectionState);
    $stateProvider.state(todoState);
    $stateProvider.state(profilState);
    $stateProvider.state(erreurState);

   $urlRouterProvider.otherwise(function ($injector) {
        var $state = $injector.get('$state');
        $state.go('404');
    });

});

monApp.run(['$cookies','$state', '$http', function($cookies,$state,$http) {
    $state.go('include');
}]);