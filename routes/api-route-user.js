var dataLayer = require('../layers/dataLayer-user');
var bcrypt = require('bcrypt');
var saltRounds = 10;

var appRouter = function (router) {
    
    router.get('/',function(req,res){
        res.sendFile('../public/index.html')
    });

    router.post('/api/user/find', function(req, res){
        dataLayer.findOne( {
            userName: req.body.userName,
        }, function(user){
            if(user == null || !bcrypt.compareSync(req.body.password,user.password) )
                res.status(800).send("Nom de compte ou mot de passe incorrect");
            else
                user = {
                    _id : user._id,
                    userName : user.userName
                };
                res.send(user);
        });
    });

    router.post('/api/user/exist', function(req, res){
        dataLayer.findOne( {
            userName: req.body.collabo,
        }, function(user){
            res.send((user != null));
        });
    });

    router.post('/api/user/create', function(req, res){
            dataLayer.findOne( {
                userName: req.body.userName
            }, function(user){
                if(user == null)

                    bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                        dataLayer.create({
                            userName: req.body.userName,
                            password: hash
                        }, function(){
                            res.send("Compte bien crée");
                            
                        });
                    });
                else
                    res.status(801).send("Nom d'utilisateur déja pris");
            });

        });

    


}
  
module.exports = appRouter;