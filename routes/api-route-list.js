var dataLayer = require('../layers/dataLayer-list');

var appRouter = function (router) {
    router.get('/',function(req,res){
        res.sendFile('../public/index.html')
    });

    router.post('/api/laliste', function(req, res){
        dataLayer.find({listID : req.body.listID},function(laliste){
            res.send(laliste);
        });
    });

    router.post('/api/laliste/create', function(req, res){
        if(req.body.auteur == undefined) req.body.auteur = "inconnu";
        dataLayer.create({
            listID: req.body.listID,
            taskName: req.body.taskName,
            done: 0,
            date: Date.now(),
            auteur: req.body.auteur
        }, function(laliste){
            dataLayer.find({listID : req.body.listID}, function(laliste){
                res.send(laliste);
            });
        });
    });

    router.post('/api/laliste/delete', function(req, res){
        dataLayer.deleteOne({
            _id: req.body._id
        }, function(laliste){
            dataLayer.find({listID : req.body.listID},function(laliste){
                res.send(laliste);
            });
        });
    });

    //A FAIRE
    router.post('/api/laliste/deleteList', function(req, res){
        dataLayer.delete({
            listID: req.body.listeId
        }, function(laliste){
            dataLayer.find({listID : req.body.listID},function(laliste){
                res.send(laliste);
            });
        });
    });

    router.put('/api/laliste/done', function(req, res){
        dataLayer.updateOne({
            _id: req.body._id
        },{
            $bit: {done: {xor: 1}}
        }, function(laliste){
            dataLayer.find({listID : req.body.listID},function(laliste){
                res.send(laliste);
            });
        });
    });

    router.put('/api/laliste/update', function(req, res){
        dataLayer.updateOne({
            _id: req.body._id
        },{
            $set: {taskName : req.body.newName, auteur : req.body.auteur}
        }, function(laliste){
            dataLayer.find({listID : req.body.listID},function(laliste){
                res.send(laliste);
            });
        });
    });
}
  
module.exports = appRouter;