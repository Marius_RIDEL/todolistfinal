var dataLayer = require('../layers/dataLayer-profil');

var appRouter = function (router) {
    
    router.get('/',function(req,res){
        res.sendFile('../public/index.html')
    });

    router.get('/api/leslistes/perso', function(req, res){
        var user = undefined;
            
            if(req.cookies.user === undefined){
                
                user = req.headers.authorization;
            } else {
                user = JSON.parse(req.cookies.user)._id;
            }
            console.log(user);
        var auteur = {auteur : user};
        dataLayer.find( auteur, function(listes){
            console.log(listes);
            res.send(listes);
        });
    });

    router.get('/api/leslistes/collab', function(req, res){
        var user = undefined;
            
            if(req.cookies.user === undefined){
                
                user = req.headers.authorization;
            } else {
                user = JSON.parse(req.cookies.user).userName;
            }
        var collabo = {collabo : user};
        dataLayer.findCollab( collabo, function(listes){
            res.send(listes);
        });
    });

    router.post('/api/leslistes/create', function(req, res){
        dataLayer.create({
            auteur: req.body.auteur,
            name: req.body.listName,
            collabo: []
        }, function(listes){

            var user = undefined;
            
            if(req.cookies.user === undefined){
                
                user = req.headers.authorization;
            } else {
                user = JSON.parse(req.cookies.user)._id;
            }
            var auteur = {auteur : user};
            dataLayer.find(auteur,function(listes){
                res.send(listes);
            });
        });
    });

    router.put('/api/leslistes/addCollab', function(req, res){
        dataLayer.addCollab(req.body._id,req.body.collabo, function(listes){
            var user = undefined;
            
            if(req.cookies.user === undefined){
                
                user = req.headers.authorization;
            } else {
                user = JSON.parse(req.cookies.user)._id;
            }

            var auteur = {auteur : user};
            dataLayer.find(auteur,function(listes){
                res.send(listes);
            });
        });
    });

    router.put('/api/leslistes/removeCollab', function(req, res){

        dataLayer.removeCollab(req.body._id,req.body.collabo, function(listes){
           
            var user = undefined;
            
            if(req.cookies.user === undefined){
                
                user = req.headers.authorization;
            } else {
                user = JSON.parse(req.cookies.user).userName;
            }

            if(user == req.body.collabo){
                var collabo = {collabo : user};
                dataLayer.findCollab( collabo, function(listes){
                    console.log(listes);
                    res.send(listes);
                });
            }else{

                var auteur = {auteur : user};
                dataLayer.find( auteur, function(listes){
                    console.log(listes);
                    res.send(listes);
                });
            }   
        });
    });

    router.post('/api/leslistes/delete', function(req, res){
        dataLayer.deleteOne({
            _id: req.body._id
        }, function(laliste){

            var user = undefined;
            
            if(req.cookies.user === undefined){
                
                user = req.headers.authorization;
            } else {
                user = JSON.parse(req.cookies.user)._id;
            }
            
            var auteur = {auteur : user};
            dataLayer.find(auteur,function(listes){
                res.send(listes);
            });
        });
    });


}
  
module.exports = appRouter;