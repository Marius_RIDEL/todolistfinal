var serverNode = "http://todolist-ridel.herokuapp.com/api";

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var monApp = angular.module('monApp', ['ionic','ui.router'])

monApp.run(function($ionicPlatform, $state) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }
    $state.go('include');
  });
})

monApp.config(function($locationProvider,$stateProvider, $urlRouterProvider) {  
  $locationProvider.hashPrefix('');
  
  var includeState = {
    name: "include",
    url:"/",
    views: {
        'header@': {
        },
        'content@': {
            controller: "includeCtrl"
        }
    }
  };

  var connectionState = {
      name: "connection",
      url:"/connection",
      views: {
          'header@': {

          },
          'content@': {
              templateUrl: "connection.html",
              controller: "connectionCtrl"
          }
      }
  };

  var todoState = {
      name: "todo",
      url:"/todo",
      params: {
          listeID: null,
          listeName: null
      },
      views: {
          'header@': {
              templateUrl: 'navbar.html',
              controller: 'navCtrl'
          },
          'content@': {
              templateUrl: "todo.html",
              controller: "toDoCtrl"
          }
      }
  };

  var profilState = {
      name: "profil",
      url:"/profil",
      views: {
          'header@': {
              templateUrl: 'navbar.html',
              controller: 'navCtrl'
          },
          'content@': {
              templateUrl: 'profil.html',
              controller: 'profilCtrl'
          }
      }
  };

  var erreurState = {
      name: "404",
      url:"/error",
      views: {
          'header@': {
          },
          'content@': {
              templateUrl: "404.html",
          }
      }
  };

  $stateProvider.state(includeState);
  $stateProvider.state(connectionState);
  $stateProvider.state(todoState);
  $stateProvider.state(profilState);
  $stateProvider.state(erreurState);

  $urlRouterProvider.otherwise(function ($injector) {
      var $state = $injector.get('$state');
      $state.go('404');
  });
});
