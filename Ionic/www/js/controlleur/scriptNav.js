monApp.controller('navCtrl',['$scope','$window','$state', function($scope,$window,$state){
    
    $scope.userName = JSON.parse($window.localStorage.getItem('user')).userName;

    $scope.retourChoixListe = function(){
        $state.go('profil');
    };

    $scope.deconnexion = function(){
        $window.localStorage.removeItem('user');
        $state.go('connection');
    };
}]);