monApp.controller('profilCtrl',['$scope','$window','$http', '$state', function($scope,$window,$http,$state){

    $scope.idUser = JSON.parse($window.localStorage.getItem('user'))._id;
    $scope.userName = JSON.parse($window.localStorage.getItem('user')).userName;

    $scope.formData = {};

    $http({
        method: 'get',
        url: serverNode + '/leslistes/perso',
        headers: {
            'Authorization': $scope.idUser
        }

    }).then(function successCallback(response){
        $scope.liste = response.data;
    },
    function errorCallback(response){
        console.log(response.data);
    });

    $http({
        method: 'get',
        url: serverNode + '/leslistes/collab',
        headers: {
            'Authorization': $scope.userName
        }
    }).then(function successCallback(response){
        $scope.listec = response.data;
    },
    function errorCallback(response){
        console.log(response.data);
    });

    $scope.monModal = function (id, isClosed){
        if(isClosed){
            $(id).show();
        } 
        else {
            $(id).hide();
        }
    };

    $scope.addList = function (){
        if ($scope.formData.listName != undefined) {
            $scope.formData.auteur = $scope.idUser;
            $http({
                method: 'post',
                url: serverNode + '/leslistes/create',
                data: $scope.formData,
                headers: {
                    'Authorization': $scope.idUser
                }
            }).then(function successCallback(response){
                $scope.formData ={};
                $scope.liste = response.data;
            },
            function errorCallback(response){
                console.log(response.data);
            });
        }
    };

    $scope.removeList = function (id){
        var req = {
            _id: id,
        }
        $http({
            method: 'post',
            url: serverNode + '/leslistes/delete',
            data: req,
            headers: {
                'Authorization': $scope.idUser
            }
        }).then(function successCallback(response){
            $scope.liste = response.data;
        },
        function errorCallback(response){
            console.log(response.data);
        });
    };

    $scope.addCollabo = function (list){
        
        if ($scope.formData.collabo != undefined) {
           
            var data = {_id : list._id ,collabo : $scope.formData.collabo};
            
            $http({
                method: 'post',
                url: serverNode + '/user/exist',
                data: data,
                headers: {
                    'Authorization': $scope.idUser
                }
            }).then(function successCallback(response){
                
                if(!list.collabo.includes(data.collabo) && data.collabo != $scope.userName){
                    if(response.data){

                        $http({
                            method: 'put',
                            url: serverNode + '/leslistes/addCollab',
                            data: data,
                            headers: {
                                'Authorization': $scope.idUser
                            }
                        }).then(function successCallback(response){
                            $scope.formData ={};
                            $scope.liste = response.data;
                        },
                        function errorCallback(response){
                            console.log(response.data);
                        });

                    } else {
                        $scope.formData ={};
                        alert("Utilisateur inexistant.");
                    }

                } else {
                    $scope.formData ={};
                    alert("Collaborateur déja présent.");
                }

            },
            function errorCallback(response){
                console.log(response.data);
            });

        }
    };

    $scope.removeCollabo = function (list,user){;
        var self;
        if(user != $scope.userName){
            self = $scope.idUser;
        } else {
            self = $scope.username;
        }
        var data = {_id : list ,collabo : user};
        console.log(data);
        $http({
            method: 'put',
            url: serverNode + '/leslistes/removeCollab',
            data: data,
            headers: {
                'Authorization':self
            }
        }).then(function successCallback(response){
            if(data.collabo == $scope.userName){
                $scope.listec = response.data;
            } else {
                $scope.liste = response.data;
            }
            
        },
        function errorCallback(response){
            console.log(response.data);
        });
    };

}]);