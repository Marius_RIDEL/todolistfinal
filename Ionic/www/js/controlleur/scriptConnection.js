monApp.controller('connectionCtrl',['$scope','$http','$window','$state', function($scope,$http,$window,$state){
    console.log('bolo');
    $scope.formData = {};

    $scope.demandeConnexion = function(){

        if ($scope.formData.userName != undefined && $scope.formData.password != undefined) {

            $http({
                method: 'POST',
                url: serverNode + '/user/find',
                data: $scope.formData
            }).then(function successCallback(response){
                $scope.formData = {};
                
                $window.localStorage.setItem('user',JSON.stringify(response.data));
                connecte = true;

                $state.go('profil');
            },
            function errorCallback(response){
                alert(response.data);
            });
        }
        else{
            alert("Veuillez remplir tout les champs");
        }

        
    };

    $scope.inscription = function(){

        if ($scope.formData.userName != undefined && $scope.formData.password != undefined) { 

             $http({
                method: 'POST',
                url: serverNode + '/user/create',
                data: $scope.formData
            }).then(function successCallback(response){
                $scope.formData = {};
                alert(response.data);

            },
            function errorCallback(response){
                alert(response.data);
            });
        }
        else{
            alert("Veuillez remplir tout les champs");
        }

    };
    
}]);