monApp.controller('includeCtrl',['$window', '$state', function($window,$state){
    
    var user = $window.localStorage.getItem('user');
    if(user != null) {
        $state.go('profil');
    }
    else {
        $state.go('connection');
    }
}]);