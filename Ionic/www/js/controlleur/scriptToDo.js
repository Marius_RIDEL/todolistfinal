
monApp.controller('toDoCtrl',['$scope','$http','$window','$stateParams', '$state',function($scope,$http,$window,$stateParams,$state){
    
    $scope.formData = {};
    console.log($stateParams);
    $scope.listID = { listID : $stateParams.listeID};
    $scope.listName = { listName : $stateParams.listeName};
    if($scope.listID.listID == null)$state.go('profil');

    $http({
        method: 'post',
        url: serverNode + '/laliste',
        data: $scope.listID
    }).then(function successCallback(response){
        $scope.laliste = response.data;
    },
    function errorCallback(response){
        console.log(response.data);
    });

    $scope.addTask = function (){
        if ($scope.formData.taskName != undefined) {
            $scope.formData.listID = $scope.listID.listID;
            $scope.formData.auteur = JSON.parse($window.localStorage.getItem('user')).userName;
            $http({
                method: 'post',
                url: serverNode + '/laliste/create',
                data: $scope.formData
            }).then(function successCallback(response){
                $scope.formData ={};
                $scope.laliste = response.data;
            },
            function errorCallback(response){
                console.log(response.data);
            });
        }
    };

    $scope.removeTask = function (id){
        var req = {
            _id: id,
            listID: $scope.listID.listID
        }
        $http({
            method: 'post',
            url: serverNode + '/laliste/delete',
            data: req
        }).then(function successCallback(response){
            $scope.laliste = response.data;
        },
        function errorCallback(response){
            console.log(response.data);
        });
    };

    $scope.doneTask = function (id){
        var req = {
            _id: id,
            listID: $scope.listID.listID
        }

        $http({
            method: 'put',
            url: serverNode + '/laliste/done',
            data: req
        }).then(function successCallback(response){
            $scope.laliste = response.data;
        },
        function errorCallback(response){
            console.log(response.data);
        });
    };

    $scope.updateTask = function (id, newName){
        if (newName != "") {
            var req = {
                _id: id,
                newName: newName,
                auteur: JSON.parse($window.localStorage.getItem('user')).userName,
                listID: $scope.listID.listID
            }
            $http({
                method: 'put',
                url: serverNode + '/laliste/update',
                data: req
            }).then(function successCallback(response){
                $scope.laliste = response.data;
            },
            function errorCallback(response){
                console.log(response.data);
            });
        }
    };
}]);
